const { Component } = require("react");

class CountClick extends Component{
    constructor(props){
        super(props);

        this.state = {
            count : 0
        }

        
    }

    
    buttonClickHandler = () => {
        this.setState({
            count : this.state.count + 1
        });
    }

    render(){
        return(
            <>
                <div>
                    <button onClick={this.buttonClickHandler}>Click Here</button>
                    <br/>
                    <p>You Clicked {this.state.count} Times</p>
                </div>
            </>
        )
    }
}

export default CountClick